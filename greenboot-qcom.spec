Name:               greenboot-qcom
Version:            1.0
Release:            1%{?dist}
Summary:            Greenboot scripts for Qualcomm boards with A/B boot

License:            LGPLv2+
URL:                https://gitlab.com/CentOS/automotive/rpms/greenboot-qcom

Source1:            greenboot-qcom
Source2:            greenboot-qcom-set-success.service
Source3:            greenboot-qcom-check-slot.service

BuildArch:          noarch
BuildRequires:      systemd-rpm-macros
%{?systemd_requires}
Requires:           greenboot


%description
Greenboot scripts for Qualcomm boards with A/B boot that will propagate the
boot status as the firmware expects.


%install
mkdir -p %{buildroot}%{_libexecdir}/greenboot
install -DpZm 0755 %{SOURCE1} %{buildroot}%{_libexecdir}/greenboot/greenboot-qcom
install -DpZm 0644 %{SOURCE2} %{buildroot}%{_unitdir}/greenboot-qcom-set-success.service
install -DpZm 0644 %{SOURCE3} %{buildroot}%{_unitdir}/greenboot-qcom-check-slot.service


%files
%{_libexecdir}/greenboot/greenboot-qcom
%{_unitdir}/greenboot-qcom-set-success.service
%{_unitdir}/greenboot-qcom-check-slot.service


%post
%systemd_post greenboot-qcom-set-success.service
%systemd_post greenboot-qcom-check-slot.service


%preun
%systemd_preun greenboot-qcom-set-success.service
%systemd_preun greenboot-qcom-check-slot.service


%postun
%systemd_postun_with_restart greenboot-qcom-set-success.service
%systemd_postun_with_restart greenboot-qcom-check-slot.service


%changelog
* Thu Sep 21 2023 Eric Chanudet <echanude@redhat.com> 1.0
- Initial commit
