# Summary

This package adds a wrapper called by a Systemd service units that runs the
proprietary tool once [Greenboot](https://github.com/fedora-iot/greenboot)
completes the health-checks and Systemd reaches boot-success.target.

Qualcomm automotive boards provide an A/B slot booting mecanism to always keep
a fallback image, should an update fail for any reason. This logic is mostly
enforced by the firmware and bootloader, but the system is responsible to
report if a boot was successful.

# Installation

1. Install greenboot-qcom
```
# dnf install greenboot-qcom
```
2. Enable [Greenboot](https://github.com/fedora-iot/greenboot):
```
# systemctl enable greenboot-task-runner greenboot-healthcheck greenboot-status greenboot-loading-message redboot-auto-reboot redboot-task-runner
```
3. Enable greenboot-qcom
```
# systemctl enable greenboot-qcom-check-slot greenboot-qcom-set-success
```

# Disclaimer

The proprietary tool `abctl` is provided by the vendor and not openly
available. Until that changes, there is no `Require:` statement for it in the
specfile. Should the tool not be available on the system, no action is taken
and the service unit succeeds.
